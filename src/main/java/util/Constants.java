package util;

public class Constants {

    public static final String MAIN_CONTROLLER_SERVLET = "/main/page";

    public static final String USER_CONTROLLER_SERVLET = "/user";
    public static final String ADMIN_ORDER_STATUS_CONTROLLER_SERVLET = "/admin/order/status";
    public static final String ADMIN_NEW_CRUISE_CONTROLLER_SERVLET = "/admin/newcruise";
    public static final String ADMIN_NEW_SHIP_CONTROLLER_SERVLET = "/admin/newship";
    public static final String CRUISE_LIST_PAGINAT_SERVLET = "/admin/cruiselistpaginat";
    public static final String CRUISE_LIST_PAGINAT_SERVLET_FOR_USER = "/user/cruiselistpaginat";

    //filters
    public static final String FILTER_CRUISE = "/filter/cruise";
    public static final String FILTER_CRUISE_OUT = "/filter/cruise/out";
    public static final String FILTER_ORDER = "/filter/order";
    public static final String FILTER_ORDER_OUT = "/filter/order/out";
    public static final String FILTER_ORDER_STATUS = "/filter/order/status";
    public static final String FILTER_REGIST = "/filter/regist";
    public static final String FILTER_REGIST_OUT = "/filter/regist/out";
    public static final String FILTER_INDEX_OUT = "/filter/index/out";
    public static final String FILTER_SHIP = "/filter/ship";
    public static final String FILTER_SHIP_OUT = "/filter/ship/out";
    public static final String FILTER_CRUISE_PAGINAT = "/filter/cruise/pagination";

    //ship
    public static final String SQL_ADD_NEW_SHIP =
            "INSERT INTO Ship(capacity, number_ports_visited, staffs) VALUES (?,?,?);";
    public static final String SQL_DELETE_SHIP_BY_ID = "DELETE FROM Ship WHERE ship_id = ?;";
    public static final String SQL_UPDATE_SHIP =
            "UPDATE Ship SET capacity = ?, number_ports_visited = ?, staffs = ? WHERE ship_id = ?;";
    public static final String SQL_GET_ALL_SHIPS = "SELECT * FROM Ship";
    public static final String SQL_GET_SHIP_BY_ID = "SELECT * FROM Ship WHERE ship_id = ?";


    //cruise
    public static final String SQL_ADD_NEW_CRUISE =
            "INSERT INTO Cruise(start_date, finish_date, route, picture, FK_ship) VALUES (?,?,?,?,?);";
    public static final String SQL_DELETE_CRUISE_BY_ID = "DELETE FROM Cruise WHERE cruise_id = ?;";
    public static final String SQL_DELETE_CRUISE_BY_SHIP_ID =
            "DELETE FROM Cruise WHERE FK_ship = ?;";
    public static final String SQL_UPDATE_CRUISE =
            "UPDATE Cruise SET start_date = ?, finish_date = ?, route = ?, picture = ?, FK_ship = ? WHERE cruise_id = ?";
    public static final String SQL_GET_ALL_CRUISES = "SELECT * FROM Cruise";
    public static final String SQL_GET_CRUISE_BY_ID = "SELECT * FROM Cruise WHERE cruise_id = ?";
    public static final String SQL_GET_SHIP_BY_DATE =
            "SELECT FK_ship FROM Cruise WHERE start_date = ?";
    public static final String SQL_GET_CRUISEIDS_BY_DATE = "SELECT cr.cruise_id FROM Cruise AS cr where cr.start_date = ?";
    public static final String SQL_GET_CRUISE_ID_BY_DATE_ROUTE = "SELECT cr.cruise_id FROM Cruise AS cr INNER JOIN Ship AS sh ON cr.FK_ship = sh.ship_id WHERE cr.start_date = ? AND sh.routes = ?";
    public static final String SQL_GET_CRUISE_ID_BY_DATE_ROUTE2 = "SELECT cr.cruise_id FROM Cruise AS cr WHERE cr.start_date = ? AND cr.route = ?";
    //user
    public static final String SQL_ADD_NEW_USER =
            "INSERT INTO User(first_name, last_name, administrator, login, password_) VALUES (?,?,?,?,?);";
    public static final String SQL_DELETE_USER_BY_ID = "DELETE FROM User WHERE user_id = ?;";
    public static final String SQL_UPDATE_USER =
            "UPDATE User SET first_name = ?, last_name = ?, administrator = ?, login = ?, password_ = ? WHERE user_id = ?;";
    public static final String SQL_GET_ALL_USERS = "SELECT * FROM User";
    public static final String SQL_COUNT_USERS_WITH_LOGIN = "SELECT COUNT(*) FROM User u WHERE u.login = ? AND " +
            " u.password_ = ?";

    public static final String SQL_IS_ADMIN_USER = "SELECT u.administrator FROM User u WHERE u.login = ? AND u.password_ = ?";
    public static final String SQL_GET_USER_BY_ID = "SELECT * FROM User WHERE user_id = ?";
    public static final String SQL_GET_USER_ID_BY_LOG_PAS = "SELECT u.user_id FROM User u WHERE u.login = ? AND u.password_ = ?";
    public static final String SQL_GET_CRUISE_LIST_PRG =  "select SQL_CALC_FOUND_ROWS * from cruise limit ?, ?";
    public static final String SQL_FOUND_ROWS_LIST_PRG = "SELECT FOUND_ROWS()";
    public static final String SQL_GET_CRUISE_LIST_FOR_USER_PRG = "SELECT SQL_CALC_FOUND_ROWS * FROM cruise WHERE cruise.start_date >= CURDATE() limit ?, ?";
    //order
    public static final String SQL_ADD_NEW_ORDER =
            "INSERT INTO Order_(FK_user, FK_cruise, status, date) VALUES (?,?,?,?);";
    public static final String SQL_DELETE_ORDER_BY_ID = "DELETE FROM Order_ WHERE order_id = ?;";
    public static final String SQL_UPDATE_ORDER =
            "UPDATE Order_ SET FK_user = ?, FK_cruise = ?, status = ?, date = ? WHERE order_id = ?";
    public static final String SQL_GET_ALL_ORDERS = "SELECT * FROM Order_";
    public static final String SQL_GET_ORDER_BY_ID = "SELECT * FROM Order_ WHERE order_id = ?";
    public static final String SQL_GET_NUM_PAID_ORDERS_BY_CRUISEID =
            "SElECT COUNT(or_.order_id) AS count_ids FROM Order_ AS or_ INNER JOIN Cruise AS cr ON cr.cruise_id=or_.FK_cruise WHERE or_.status = 'PAID' AND cr.cruise_id = ?";
    public static final String SQL_GET_USERSIDS_FROM_ORDER_BY_CRUISEID =
            "SELECT DISTINCT us.user_id FROM Order_ AS ord INNER JOIN Cruise AS cr ON ord.FK_cruise = cr.cruise_id INNER JOIN User AS us ON ord.FK_user = us.user_id WHERE ord.FK_cruise = ?";
    public static final String SQL_GET_ORDERSIDS_BY_CRUISEID_AND_USERID =
            "SELECT  ord.order_id FROM Order_ as ord INNER JOIN Cruise AS cr ON ord.FK_cruise = cr.cruise_id INNER JOIN User AS us ON ord.FK_user = us.user_id WHERE ord.FK_cruise = ? AND ord.FK_user = ?";
    public static final String SQL_GET_ORDERSIDS_BY_CRUISE_FINISH_DATE = "SElECT or_.order_id FROM Order_ AS or_ INNER JOIN Cruise AS cr ON cr.cruise_id = or_.FK_cruise WHERE cr.finish_date = ?";

    public static final String COULD_NOT_PERSIST_CRUISE = "Could not persist Cruise";
    public static final String CREATING_CRUISE_FAILED_NO_ID_OBTAINED = "Creating cruise failed, no ID obtained.";
    public static final String COULD_NOT_PERSIST_ORDER = "Could not persist order";
    public static final String COULD_NOT_PERSIST_SHIP = "Could not persist ship";
    public static final String COULD_NOT_PERSIST_USER = "Could not persist user";
    public static final int ROWS_PER_PAGE = 5;
    public static final int ROWS_PER_PAGE_FOR_USER = 2;
}
