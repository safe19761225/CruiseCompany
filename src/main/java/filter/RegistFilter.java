package filter;

import dao.impl.UserDAOImpl;
import domain.User;
import domain.builder.UserBuilder;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import service.UserService;
import service.impl.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


import static util.Constants.FILTER_REGIST;

/**
 * The {@code RegistFilter} class filter, that is responsible for building new user from request
 */
@WebFilter(urlPatterns = FILTER_REGIST)
public class RegistFilter implements Filter {

    private final UserService SERVICE;
    private static final Logger LOGGER = LogManager.getLogger(RegistFilter.class);

    public RegistFilter() {
        super();
        SERVICE = new UserServiceImpl(new UserDAOImpl());
    }

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain filterChain) throws IOException {

        request.setCharacterEncoding("UTF-8");
        final HttpServletResponse res = (HttpServletResponse) response;
        String firstName = request.getParameter("first name");
        String lastName = request.getParameter("last name");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String lang = request.getParameter("Lang");

        User user = new UserBuilder()
                .buildFirstName(firstName)
                .buildLastName(lastName)
                .buildAdministrator(false)
                .buildLogin(login)
                .buildPassword(password)
                .build();

        boolean lengthMatch = login.length() >= 5 && password.length() >= 5;
        int newUserId = 0;
        try {
            if (lengthMatch) {
                newUserId = SERVICE.addNewUser(user);
            }
            res.sendRedirect("/filter/regist/out?newUserId=" + newUserId +
                    "&lengthMatch=" + lengthMatch + "&Lang=" + lang);
        } catch (SQLException e) {
            LOGGER.info("User can't be added");
            res.sendRedirect("/filter/regist/out?newUserId=" + 0 +
                    "&lengthMatch=" + lengthMatch + "&Lang=" + lang);
            res.setStatus(406);
        }
        res.setStatus(200);
    }
}
