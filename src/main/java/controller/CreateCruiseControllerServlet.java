package controller;



import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static util.Constants.ADMIN_NEW_CRUISE_CONTROLLER_SERVLET;

/**
 * The {@code CreateCruiseControllerServlet} class servlet,
 * that is responsible for processing admin's requests to create new cruise
 */
@WebServlet(urlPatterns = ADMIN_NEW_CRUISE_CONTROLLER_SERVLET)
public class CreateCruiseControllerServlet extends HttpServlet {
    /**
     * Processes post-request
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException when process servlet fails
     * @throws IOException      when process servlet fails
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String lang = request.getParameter("Lang");
        request.setAttribute("Lang", lang);
        if (lang.equals("Ukr")) {
            request.getServletContext().getRequestDispatcher("/adm_add_new_cruise_ukr.jsp").forward(request, response);
        } else {
            request.getServletContext().getRequestDispatcher("/adm_add_new_cruise.jsp").forward(request, response);
        }
    }
}
