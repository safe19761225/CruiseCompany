<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Cruise Company</title>

</head>
<body>

<div align="center" style="font-size: 20px; color: green; font-family: arial, helvetica, sans-serif">
    <h2>Оберіть маршрут круїзу:</h2>

    <form action="http://localhost:9999/admin/order/status" method="post">
        <table border="1" cellpadding="5">
            <tr id="cruiseRoute">
                <th>ID</th>
                <th>Маршрут</th>
                <th>Дата початку</th>
                <th>Корабель</th>
            </tr>
            <c:forEach var="cruise" items="${cruiseListPag}">
                <tr >
                    <td><c:out value="${cruise.getId()}"/></td>
                    <td><c:out value="${cruise.getRoute()}"/></td>
                    <td><c:out value="${cruise.getStartDate()}"/></td>
                    <td><c:out value="${cruise.getShipId()}"/></td>
                    <td><img src="<c:url value="${cruise.getPicture()}"/>"></td>
                    <td>
                        <input type="hidden" name="Lang" value="${Lang}">
                        <input type="submit" name="cruiseRoute" value="${cruise.getId()}"/>
                        <%--<input type="submit" value="Обрати маршрут круїзу" />--%>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </form>

    <form action="http://localhost:9999/admin/order/status" method="post">
        <p id="result"></p>
        <select name="cruiseRoute">
            <c:forEach items="${cruiseListPag}" var="cruise">
                <option value="${cruise.getId()}">${cruise.getRoute()} on ${cruise.getStartDate()}</option>
            </c:forEach>
        </select>
        <br/><br/>
        <input type="hidden" name="Lang" value="${Lang}">
        <c:if test="${currentPage != 1}">
            <td><a href="http://localhost:9999/admin/cruiselistpaginat?page=${currentPage - 1}&Lang=${Lang}"
                   style="font-size: 15px; color: blue; font-family: arial, helvetica, sans-serif">Попередня</a></td>
        </c:if>
        <c:if test="${currentPage lt noOfPages}">
            <td><a href="http://localhost:9999/admin/cruiselistpaginat?page=${currentPage + 1}&Lang=${Lang}"
                   style="font-size: 15px; color: blue; font-family: arial, helvetica, sans-serif">Наступна</a></td>
        </c:if>
        <input type="submit" value="Обрати маршрут круїзу"/>
    </form>
    <br/><br/>
    <div align="center">
        <form action="http://localhost:9999/admin/newcruise" method="post">
            <input type="hidden" name="Lang" value="${Lang}">
            <input type="submit" value="Додати новий круїз"/>
        </form>
        <form action="http://localhost:9999/admin/newship" method="post">
            <input type="hidden" name="Lang" value="${Lang}">
            <input type="submit" value="Додати новий корабель"/>
        </form>
    </div>
</div>

</body>
</html>