<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Select StartDate and Duration of Cruise - CodeJava.net</title>
</head>
<body>

<div align="center">
    <h2>Select StartDate and Duration of Cruise</h2>

    <form action="http://localhost:9999/user/cruiselistpaginat" method="post">
        <%--<input type="hidden" name="userId" value="${userId}">--%>
        <table border="1" cellpadding="5">
            <tr>
                <th>ID</th>
                <th>Route</th>
                <th>Start Date</th>
                <th>Ship</th>
            </tr>
            <c:forEach var="cruise" items="${cruiseListPag}">
                <tr>
                    <td><c:out value="${cruise.getId()}"/></td>
                    <td><c:out value="${cruise.getRoute()}"/></td>
                    <td><c:out value="${cruise.getStartDate()}"/></td>
                    <td><c:out value="${cruise.getShipId()}"/></td>
                    <td><img src="<c:url value="${cruise.getPicture()}"/>"></td>
                </tr>
            </c:forEach>
        </table>
    </form>
    <%--<br/><br/>--%>
    <input type="hidden" name="Lang" value="${Lang}">
    <input type="hidden" name="userId" value="${userId}">
    <c:if test="${currentPage != 1}">
        <td><a href="http://localhost:9999/user/cruiselistpaginat?page=${currentPage - 1}&Lang=${Lang}"
               style="font-size: 15px; color: blue; font-family: arial, helvetica, sans-serif">Previous</a>
        </td>
    </c:if>
    <c:if test="${currentPage lt noOfPages}">
        <td>
            <a href="http://localhost:9999/user/cruiselistpaginat?page=${currentPage + 1}&Lang=${Lang}"
               style="font-size: 15px; color: blue; font-family: arial, helvetica, sans-serif">Next</a>
        </td>
    </c:if>
    <br/><br/>
    <%--<input type="submit" value="selected cruise route"/>--%>

    <form action="http://localhost:9999/user" method="post">

        Select a startdate:&nbsp;
        <select name="startdate">
            <c:forEach items="${cruiseListPag}" var="cruise">
                <option value="${cruise.getStartDate()}">${cruise.getStartDate()}</option>
            </c:forEach>
        </select>
        <br/><br/>
        Select a Duration:&nbsp;
        <select name="cruiseDuration">
            <c:forEach items="${cruiseListPag}" var="cruise">
                <option value="${cruise.getDuration()}">${cruise.getDuration()} days</option>
            </c:forEach>
        </select>
        <input type="hidden" name="userId" value="${userId}">
        <input type="hidden" name="Lang" value="${Lang}">
        <br/><br/>
        <input type="submit" value="Submit date and duration"/>
    </form>
</div>
</body>
</html>