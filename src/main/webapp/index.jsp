<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<body>

<form action="http://localhost:9999/auth" method="post">
    <div class="background-image">
        <div style="font-size: 80px;
         -webkit-text-stroke-width: 1.5px;
         -webkit-text-stroke-color: #B80000;
         -webkit-text-fill-color: #8ADAFF;
         font-family: Impact, Charcoal, sans-serif;
         text-shadow: 4px 4px 13px #A3A3A3;
         font-style: italic;">
            <ex:Hello/>
        </div>
        <%--<img src="<c:url value="/pictures/ship.jpg"/>">--%>
        <table>
            <tr>
                <td style="font-size: 15px; color: lightcoral; font-family: arial, helvetica, fantasy">Lang:</td>
                <td><select name="Lang">
                    <option value="Eng">Eng</option>
                    <option value="Ukr">Ukr</option>
                </select></td>
            </tr>
            <tr>
                <td style="font-size: 15px; color: lightcoral; font-family: arial, helvetica, fantasy">Login:</td>
                <td><input type="text" name="login" align="left"/></td>
            </tr>
            <tr>
                <td style="font-size: 15px; color: lightcoral; font-family: arial, helvetica, fantasy">Password:</td>
                <td><input type="password" name="password" align="left"/></td>
            </tr>
        </table>
        <input style="font-size: 15px; color: darkred; font-family: arial, helvetica, fantasy" type="submit"
               value="login"/>
    </div>
</form>
</body>

<style>
    * {
        margin: 0;
        padding: 0;
    }

    .background-image {
        background-image: url("/pictures/ship8.jpg");
        background-size: cover;
        background-repeat: no-repeat;
        height: 100vh;
    }

</style>
</html>
